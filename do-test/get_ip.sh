json() {
    python3 -c "import json;print(json.load(open('$1'))$2)"
}

IP_ADDR=$(json terraform.tfstate "['resources'][0]['instances'][0]['attributes']['ipv4_address']")
echo $IP_ADDR

echo "export IP_ADDR=$IP_ADDR" > .droplet-ip
