source load_env.sh

IP_ADDR=$(source get_ip.sh)
echo connecting to $IP_ADDR

ssh $REMOTE_USER@$IP_ADDR -oStrictHostKeyChecking=no
