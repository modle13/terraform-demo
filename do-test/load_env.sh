source .env

expected_vars=(REMOTE_USER REMOTE_PASS ROOT_USER DO_TOKEN)

error() {
    echo "$1 is required; did you forget to create a .env file?"
    exit 1
}

check_var() {
    # cmon bash, what is this garbage
    if [ -z "${!1}" ]; then
        error $1
    fi
}

for v in ${expected_vars[@]}; do
    check_var "$v"
done
