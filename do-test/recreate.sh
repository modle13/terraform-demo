# exit when any command fails
set -e

source load_env.sh

source run_terraform.sh destroy
source run_terraform.sh apply
source test_connection.sh
source install.sh
source test_ansible.sh
source configure_access.sh
source deploy.sh
# source deploy.sh
# source connect_with_user.sh
