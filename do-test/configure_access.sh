echo configuring users

source load_env.sh

ansible-playbook ansible/configure_access.yml -i $(source get_ip.sh), -e ssh_user=$ROOT_USER
