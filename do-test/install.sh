# only include dependencies needed to run ansible
# everything else should be handled by ansible itself

# run on client workstation

IP_ADDR=$(source get_ip.sh)
echo connecting to $IP_ADDR

install_script=install/python-install.sh
target_user=root

echo installing dependencies:
ssh $target_user@$IP_ADDR -oStrictHostKeyChecking=no cat $install_script
ssh $target_user@$IP_ADDR -oStrictHostKeyChecking=no chmod u+x $install_script
ssh $target_user@$IP_ADDR -oStrictHostKeyChecking=no source $install_script
