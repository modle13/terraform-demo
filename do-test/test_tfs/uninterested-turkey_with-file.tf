# https://www.terraform.io/docs/providers/do/index.html

# Set the variable value in *.tfvars file
# or using -var="do_token=..." CLI option
variable "do_token" {}
variable "pvt_key" {}

# Configure the DigitalOcean Provider
provider "digitalocean" {
  token = "${var.do_token}"
}

resource "digitalocean_droplet" "web" {
  name = "uninterested-turkey"
  region = "nyc3"
  size = "s-1vcpu-1gb"
  image = "ubuntu-18-04-x64"
  ssh_keys = ["42:3b:57:58:dc:9d:c5:e4:b4:3e:ca:48:bb:7e:2a:b7"]

  provisioner "file" {
    source      = "myapp/myapp.conf"
    destination = "/root/text/myapp.conf"

    connection {
      user     = "root"
      host     = "${self.ipv4_address}"
      type     = "ssh"
      password = "${file(var.pvt_key)}"
    }
  }
}

