variable "do_token" {}

provider "digitalocean" {
  token = "${var.do_token}"
}

data "digitalocean_droplet" "example" {
  name = "uninterested-turkey"
}
