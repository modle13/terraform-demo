echo deploying apps
echo $1

source load_env.sh

if [ ! -z "$1" ]; then
    TARGET_STRING="-e target=$1"
fi

ansible-playbook ansible/deploy.yml -i $(source get_ip.sh), -e ssh_user=$REMOTE_USER -e ansible_become_pass=$REMOTE_PASS $TARGET_STRING
