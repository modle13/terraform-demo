if [ $1 != "apply" ] && [ $1 != "destroy" ]; then
    echo "expected one of: ['apply', 'destroy'], got $1"
    exit 1
fi

source load_env.sh

key_path="$HOME/.ssh/id_rsa_do"
key_exists=$(ls $key_path | wc -l)

if [ "$key_exists" -eq "0" ]; then
    echo "ssh key does not exist"
    exit 1
fi

terraform $1 -auto-approve -var "do_token=${DO_TOKEN}" -var "pvt_key=$key_path"
