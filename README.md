# offical docs

https://www.terraform.io/docs/providers/do/index.html

# testing 

docker container did not work; have to do a bunch of extra stuff beyond the hello world to get it working

make did not work; some windows line endings junk, my cleanendings.sh did not fix it https://stackoverflow.com/questions/29045140/env-bash-r-no-such-file-or-directory

```
06:45:05-matt@matt-GL752VW:~/projects/terraform/terraform$ make
/usr/bin/env: ‘bash\r’: No such file or directory
Makefile:98: recipe for target 'fmtcheck' failed
make: ** [fmtcheck] Error 127  
```

Got the binary from a zip here: https://www.terraform.io/downloads.html

did the aws example because that's what all the cool kids use: https://learn.hashicorp.com/terraform/getting-started/build

going through initial aws examples answered some questions about how to terraform in general; was able to apply this to the DO example:

needed a personal access token from here:
https://cloud.digitalocean.com/account/api/tokens?i=fe3204

```
cd do-test
terraform init
terraform apply
# to auto-approve prompts:
# terraform apply -auto-approve
```

a dir appeared on local filesystem: `terraform.tfstate`

# autocomplete

```
terraform -install-autocomplete
```

# making a file

see `copyfile.tf`

```
terraform apply -var="do_token=$DO_KEY"
```

# unneeded vars are called out

passing a var on the cli when it's not used in a `.tf` file will show this

```
Error: Value for undeclared variable

A variable named "do_token" was assigned on the command line, but the root
module does not declare a variable of that name. To use this value, add a
"variable" block to the configuration.
```

# missing resource identifiers

If you forget the identifiers of the droplet, obviously it can't find it

I'm not sure why `data` and `provisioner` have different identifier requirements; data just needed the name, provisioner needs name, region, size, image, like it's trying to create something, but I just want it to use the existing instance

```
Error: Missing required argument

  on copyfile.tf line 10, in resource "digitalocean_droplet" "example":
  10: resource "digitalocean_droplet" "example" {

The argument "name" is required, but no definition was found.


Error: Missing required argument

  on copyfile.tf line 10, in resource "digitalocean_droplet" "example":
  10: resource "digitalocean_droplet" "example" {

The argument "region" is required, but no definition was found.


Error: Missing required argument

  on copyfile.tf line 10, in resource "digitalocean_droplet" "example":
  10: resource "digitalocean_droplet" "example" {

The argument "size" is required, but no definition was found.


Error: Missing required argument

  on copyfile.tf line 10, in resource "digitalocean_droplet" "example":
  10: resource "digitalocean_droplet" "example" {

The argument "image" is required, but no definition was found.
```

# more examples, with pubkey md5, etc

https://www.digitalocean.com/community/tutorials/how-to-use-terraform-with-digitalocean

delete terraform.tfstate file to start over

# creating a droplet

`provision.tf` worked to create the droplet

I got the connection email and was able to log into the console, but it timed out at the file privision step after 5 minutes, possibly because I changed the default password from the console while it was trying to create, or because it was using 22 and I didn't give it a key (that's probably it). Error:

```
Error: timeout - last error: dial tcp :22: connect: connection refused
```

# destroy a droplet

with the provider file, run this

```
terraform destroy -var="do_token=$DO_KEY" 
```

where provider file looks like this

```
variable "do_token" {}

# Configure the DigitalOcean Provider
provider "digitalocean" {
  token = "${var.do_token}"
}

resource "digitalocean_droplet" "web" {
  name = "uninterested-turkey"
  region = "nyc3"
  size = "s-1vcpu-1gb"
  image = "ubuntu-18-04-x64"
}
```

# running in debug mode

```
export TF_LOG=DEBUG
```

# comments

`#` can be used to comment out parts of the `.tf` file

# tainting a resource

```
terraform taint
```

This will cause the resource to be replaced on the next `terraform apply`

# running uninterested turkey with file

```
. ~/do-token
terraform apply -var "do_token=${DO_TOKEN}" -var "pvt_key=$HOME/.ssh/id_rsa_do"
```
